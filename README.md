# [`dune-staging-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune-staging-git) metapackage

| `README.md`                                                                                                              | Latest build from [GitHub](https://github.com/dune-project)'s package (DD/MM/YY) | `pkg.tar.zst` package                                                                                                                                                                       |
| :----------------------------------------------------------------------------------------------------------------------- | :------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| [`dune-logging-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-logging-git/README.md)     |                                    10/04/2022                                    | [`dune-logging-git-2.8.r94.3177b90-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-staging-git/x86_64/dune-logging-git-2.8.r94.3177b90-1-x86_64.pkg.tar.zst)          |
| [`dune-typetree-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-typetree-git/README.md)   |                                    10/04/2022                                    | [`dune-typetree-git-2.9.r669.62e0978-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-staging-git/x86_64/dune-typetree-git-2.9.r669.62e0978-1-x86_64.pkg.tar.zst)      |
| [`dune-uggrid-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-uggrid-git/README.md)       |                                    10/04/2022                                    | [`dune-uggrid-git-2.9.r7714.9ee82f55-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-staging-git/x86_64/dune-uggrid-git-2.9.r7714.9ee82f55-1-x86_64.pkg.tar.zst)      |
| [`dune-functions-git`](https://gitlab.com/dune-archiso/pkgbuilds/dune/-/raw/main/PKGBUILDS/dune-functions-git/README.md) |                                    10/04/2022                                    | [` dune-functions-git-2.9.r1795.1017530-1-x86_64.pkg.tar.zst`](https://dune-archiso.gitlab.io/repository/dune-staging-git/x86_64/dune-functions-git-2.9.r1795.1017530-1-x86_64.pkg.tar.zst) |

### Tests

- `dune-logging-git` OK.
- `dune-typetree-git` OK.
- `dune-uggrid-git` OK.
- `dune-functions-git` OK.
- `python-dune-functions-git` OK.
